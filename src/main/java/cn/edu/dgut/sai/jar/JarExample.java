package cn.edu.dgut.sai.jar;

/**
 * @author Sai
 * Created by Sai on 2019/8/25.
 */
public class JarExample {

    /*
    1、使用idea的工具运行main方法
    2、使用mvn编译、打包项目
        mvn clean package
    3、观察项目目录下的target的内容
    4、查看打包生成的jar文件里的内容
        jar tf target/jar-demo-1.0-SNAPSHOT.jar
        查看jar文件中的MANIFEST.MF文件的内容，注：使用mvn编译打包虽然会自动生成主清单文件，但没有指定主类的Main-Class属性。
            unzip -p target/jar-demo-1.0-SNAPSHOT.jar META-INF/MANIFEST.MF
    5、运行jar文件
        java -jar target/jar-demo-1.0-SNAPSHOT.jar
            注：会提示没有主清单属性，运行失败。
    6、运行JarExample类，入口点是main方法
        java -cp target/jar-demo-1.0-SNAPSHOT.jar cn.edu.dgut.sai.jar.JarExample
            注：执行某个类，并使用-cp指定类路径。
    7、运行App类
        java -cp target/jar-demo-1.0-SNAPSHOT.jar cn.edu.dgut.sai.App

    --------------在pom.xml文件配置maven-jar-plugin插件，设置全路径主类--------------
    8、重新编译、打包
        mvn clean package
    9、查看jar文件中的MANIFEST.MF文件的内容
        unzip -p target/jar-demo-1.0-SNAPSHOT.jar META-INF/MANIFEST.MF
            观察主清单内容，此时已经包含Main-Class属性，值为maven-jar-plugin插件设置的主类。
    10、执行jar文件
        java -jar target/jar-demo-1.0-SNAPSHOT.jar
            注：因为在主清单文件中设置了主类，此时没有报错了，直接执行主类的main方法，并输出内容到终端。
     */
    public static void main(String[] args) {
        System.out.println("Hello 东莞理工学院!");
    }

}

